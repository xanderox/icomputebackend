require 'sinatra'
require 'sinatra/json'
require 'mysql2'
require 'json'

# class IComputeServiceApp < Sinatra::Application

	db = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "", :port => "3306", :database => "icompute")

	get '/' do	
		send_file 'welcome.html'
	end

	get '/api/appointment/:customer_id' do
		results = db.query("SELECT * FROM _orders WHERE customer_id=#{params[:customer_id]}")
		appointments = Array.new
		results.each do |row|
			employee = db.query("SELECT name,phone FROM employees WHERE id=#{row['employee_id']}").to_a[0]
			device = db.query("SELECT * FROM devices WHERE id=#{row['device_id']}").to_a[0]
			device_color = db.query("SELECT color FROM colors WHERE id=#{row['color_id']}").to_a[0]['color']

			appointment = Hash.new
			appointment[:appointment_time] = row['appointment_time']
			appointment[:employee_name] = employee['name']
			appointment[:employee_phone] = employee['phone']
			appointment[:device_color] = device_color
			appointment[:device_name] = device['name']
			appointment[:device_capacity] = device['capacity']
			appointment[:device_price] = device['price']
			appointment[:appointment_placed] = row['date']

			appointments << appointment
		end
		json appointments
	end

	get '/api/carrier' do
		results = db.query("SELECT * FROM carriers")
		carriers = Array.new
		results.each do |row|
			carriers << row['name']
		end
		json carriers
	end

	get '/api/color' do
		results = db.query("SELECT color FROM colors")
		colors = Array.new
		results.each do |row|
			colors << row['color']
		end
		json colors
	end

	get '/api/device' do
		results = db.query("SELECT * FROM devices")
		
		devices = Array.new
		results.each do |info|
			device = devices.find {|d| d[:name] == info['name']}
			if device.nil?
				device = Hash.new
				device[:name] = info['name']
				device[:capacities] = []
				device[:prices] = []
				devices << device
			end
			device[:capacities] << info['capacity']
			device[:prices] << info['price']
		end
		json devices
	end

	get '/api/employee' do
		results = db.query("SELECT name, email, phone FROM employees")
		employees = Array.new
		# results.each do |row|
		# 	employees << [row['name'], row['email'], row['phone']]
		# end 
		results.each do |row|
			employees << row['name']
		end
		json employees
	end

	get '/api/order/:employee_id' do
		results = db.query("SELECT * FROM _orders WHERE employee_id=#{params[:employee_id]}")
		orders = Array.new
		results.each do |row|
			customer = db.query("SELECT name,phone FROM customers WHERE id=#{row['customer_id']}").to_a[0]
			device = db.query("SELECT * FROM devices WHERE id=#{row['device_id']}").to_a[0]
			device_color = db.query("SELECT color FROM colors WHERE id=#{row['color_id']}").to_a[0]['color']

			order = Hash.new
			order[:appointment_time] = row['appointment_time']
			order[:customer_name] = customer['name']
			order[:customer_phone] = customer['phone']
			order[:device_color] = device_color
			order[:device_name] = device['name']
			order[:device_capacity] = device['capacity']
			order[:device_price] = device['price']
			order[:order_placed] = row['date']

			orders << order
		end
		json orders
	end

	get '/api/location/employee/:id/:lat/:long' do
		#should be a post method maybe
		#store coordinates in db 
		# coordinates have to be stored with employee name or id
		db.query("INSERT INTO locations (latitude, longitude, employee_id) VALUES ('#{params[:lat]}','#{params[:long]}','#{params[:id]}')")
	end

	get '/api/location/employee/' do 
		#must include name of employee
		locations = db.query("SELECT * FROM locations").to_a
		json locations
	end 

	post '/api/appointment' do
		#get customer id from device as user is logged in
		values = JSON.parse request.body.read 
		device_id = db.query("SELECT id FROM devices WHERE name='#{values['device_name']}' AND capacity='#{values['device_capacity']}'").to_a[0]['id']
		employee_id = db.query("SELECT id FROM employees WHERE name='#{values['employee']}'").to_a[0]['id']
		color_id = db.query("SELECT id FROM colors WHERE color='#{values['device_color']}'").to_a[0]['id']
		customer_id = 2
		appointment_time = DateTime.parse(values['appointment_time']).strftime('%Y-%m-%d %H:%M:%S')

		db.query("INSERT INTO _orders (device_id, customer_id, employee_id, color_id, appointment_time) VALUES ('#{device_id}','#{customer_id}','#{employee_id}','#{color_id}','#{appointment_time}')")
		# add error handling and send success or failure response
	end

	not_found do
		'Page not found sorry'
	end
# end

	